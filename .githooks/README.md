## What is Git hooks?

Git hooks are scripts that Git executes before or after events such as: commit, push, and receive. 
Git hooks are a built-in feature - no need to download anything. Git hooks are run locally.

## Why you need git hooks?

Git hooks is the simplest automation tools that could check and force conventions before code deployed to the team.

It could be used as part of code review process, code standards process, code deploy etc.

## What you could do with git hooks?

We use 4 main tasks that automate code review process:

* check commit messages
* run js linting
* run css/scss/sass linting
* install npm dependencies
    
Virtually you could run any tasks on hooks:

* disabling from push to some origin/branch
* deploy to some instance
* build
* check unicode symbols, file format
etc. etc.

### Why not git guppy?

Git guppy extremely overengeneered for such simple task as run script.
The only reason that you need to use git guppy is to invoke gulp task on git hooks. 

## Approach

The approach is based on placing all files in project space instead of `.git` directory.
To achieve this we copy to `.git/hooks` only bash scripts that call scripts in `.githooks` directory. 
So as the result you don't need to copy or symlink scripts into `.git` directory on each script updates.

We intentionaly do not add any helpers. This is aimed to reduce cross-script dependencies and keep all structure modular and simple.
`run-hooks` bash file and `copy-hooks.js` the only files that kind-of helpers.

## Installation

1. Copy `.githooks` directory to your project and add this line to `package.json`:

```js
"scripts": {
    "postinstall": "node .githooks/copy-hooks.js"
}
```

2. Run `npm run postinstall` to copy bash scripts to `.git` directory. You could do it manually or it will be called automatically after `npm install`.

## Hooks

* `run-hooks` 
    
    Used to run several separate scripts in one hook. This opens possibilities to store logic in different files.

* `commit-msg` 
    
    Runs `commit-msg-pattern.js`
    
    Modify regex string to match message convention. Ex:
    `^FTFC-[0-9]+: (.*)|^(Merge|Revert|Finish).+`
    
* `post-update`, `post-merge`. 
    
    Runs `post-merge-npm-update.js`

* `pre-commit`
    * Runs `pre-commit-lint-js.js`
    
    Modify regex string to match what files from staged to commit should be validated. Ex:
    `git diff --cached --name-only --diff-filter=ACMR -- app_storefront_core_ext/*.js | xargs echo`
    
    Add another task on `pre-commit`. Place script file in the `.githooks` and add file name in `pre-commit`. Ex:
    `$(pwd)/.githooks/pre-commit-lint-js.js my-new-script-also.js`. 
    
    Please note that after that you need to force team to run `npm run postinstall` or `npm update` manually to copy updated file to `.git` directory.

* `pre-push`
    
    Runs code inside it. Not very useful since Bitbucket/Github run hooks on push.

## What you need to know

* [git hooks](https://git-scm.com/docs/githooks)
* stdout/stderr exit statuses 
